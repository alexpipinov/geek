<section class="b-test-kinds i-small-pd">
	<h2 class="b-title_small b-title_white">
		<span class="i-inner-wrapper">
			Виды<br />
			тестирования
		</span>
	</h2>

	<div class="b-test-kinds__items">
		<article class="b-test-kinds__item">
			<img class="b-test-kinds__icon" src="images/icons/1.png" alt="">
			<h3 class="b-test-kinds__text">
				Smoke<br />
				тестирование
			</h3>
		</article>
		<article class="b-test-kinds__item">
			<img class="b-test-kinds__icon" src="images/icons/2.png" alt="">
			<h3 class="b-test-kinds__text">
				Sanity<br />
				тестирование
			</h3>
		</article>
		<article class="b-test-kinds__item">
			<img class="b-test-kinds__icon" src="images/icons/3.png" alt="">
			<h3 class="b-test-kinds__text">
				Приемочное<br />
				тестирование
			</h3>
		</article>
		<article class="b-test-kinds__item">
			<img class="b-test-kinds__icon" src="images/icons/4.png" alt="">
			<h3 class="b-test-kinds__text">
				Регрессионное<br />
				тестирование
			</h3>
		</article>
		<article class="b-test-kinds__item">
			<img class="b-test-kinds__icon" src="images/icons/5.png" alt="">
			<h3 class="b-test-kinds__text">
				Тестирование <br />
				совместимости
			</h3>
		</article>
	</div>

	<div class="i-btn-box">
		<button class="b-btn b-btn_contour js-popup-control">Заказать тестирование</button>
	</div>
</section>