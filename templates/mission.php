<div id="mission" class="b-mission i-big-pd">
	<div class="i-inner-wrapper">
		<h2 class="b-title_big b-title_dark">
			Наша миссия — меньше багов, <br /><span class="i-dashed js-dashed"><span class="i-dashed__text">больше покупок!</span></span>
		</h2>
		<p class="b-descr_big b-descr_dark">
			7 лет охоты за багами. Сплочённая команда тестировщиков-магистров.
			<br />Когда они вместе, у багов нет шансов
		</p>

		<div class="i-btn-box">
			<button class="b-btn b-btn_orange js-popup-control">Заказать тестирование</button>
		</div>
	</div>
</div>