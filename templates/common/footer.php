<footer class="b-footer">
	<div class="b-footer__copy">Copyright © GeekStone — Rights Reserved</div>

	<img src="images/logo/small.svg" class="b-footer__logo" alt="">
</footer>