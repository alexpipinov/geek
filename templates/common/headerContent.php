<a href="/" class="b-header__logo">
	<img src="images/logo/logo.svg" alt="">
</a>

<nav class="b-nav js-nav">
	<a class="b-nav__link js-popup-control" href="#">
		<span class="b-nav__text">Заказать тестирование</span>
	</a>
	<a class="b-nav__link js-go-to" data-target="remoteTesting" href="#">
		<span class="b-nav__text">Удаленное тестирование</span>
	</a>
	<a class="b-nav__link js-go-to" data-target="duo" href="#">
		<span class="b-nav__text">Команда</span>
	</a>
	<a class="b-nav__link js-go-to" data-target="honors" href="#">
		<span class="b-nav__text">Портфолио</span>
	</a>
	<a class="b-nav__link js-go-to" data-target="contacts" href="#">
		<span class="b-nav__text">Контакты</span>
	</a>

	<div class="b-nav__dj">
		<img src="images/nav/dj.png" alt="">
	</div>
</nav>