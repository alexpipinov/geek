<div id="popup" class="b-popup">
	<div class="b-popup__overlay js-popup-control"></div>
	<div class="b-popup__container js-form-wrapper">

		<form id="order" action="#" class="b-form">
			<div class="b-popup__title">Готовы начать тестирование?</div>

			<div class="b-form__wrapper">
				<div class="b-form__fields">
					<div class="b-form__input-wrapper">
						<input type="text" class="b-form__input js-client-input" tabindex="1" data-required name="name" placeholder="Как вас зовут?">
					</div>
					<div class="b-form__input-wrapper">
						<input type="tel" class="b-form__input js-client-input" tabindex="2" name="phone" data-required   placeholder="+7 999 123 45 67">
					</div>
					<div class="b-form__input-wrapper">
						<input type="text" class="b-form__input js-client-input" tabindex="3" name="mail" data-required  placeholder="petrov.ivan@mail.ru">
					</div>
					<div class="b-form__input-wrapper">
						<input type="text" class="b-form__input js-client-input" tabindex="4" name="product" data-required  placeholder="Укажите продукт. Например, мобильное приложение">
					</div>
					<div class="b-form__input-wrapper">
						<textarea class="b-form__input b-form__textarea js-client-input" tabindex="5" name="product_info"  data-required placeholder="Что еще нам нужно знать о вашем продукте?"></textarea>
					</div>
				</div>
	
				<div class="b-form__descr">
					<div class="b-form__conditions">
						<span id="formErrorMsg" class="b-form__error-msg">Все поля являются обязательными к заполнению.</span> После отправки заявки мы свяжемся с вами для уточнения деталей.
					</div>
	
					<div class="b-form__btn-wrapper">
						<span id="orderSubmitBtn" class="b-btn b-btn_orange">
							<span class="b-btn__text">Заказать тестирование</span>
						</span>
					</div>
				</div>
			</div>
			
			<div class="b-form__success-msg">
				<div class="b-popup__title">Ваша заявка принята.</div>
				<div>Мы свяжемся с вами в ближайшее время</div>
			</div>
		</form>

		<a href="#" class="b-popup__close js-popup-control"></a>
	</div>
</div>