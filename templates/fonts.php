<style>
	@font-face {
		font-family: 'open';
		src: url('fonts/opensans-bold-webfont.eot');
		src: url('fonts/opensans-bold-webfont.eot?#iefix') format('embedded-opentype'),
		url('fonts/opensans-bold-webfont.woff2') format('woff2'),
		url('fonts/opensans-bold-webfont.woff') format('woff'),
		url('fonts/opensans-bold-webfont.ttf') format('truetype'),
		url('fonts/opensans-bold-webfont.svg#open_sansbold') format('svg');
		font-weight: 900;
		font-style: normal;
	}

	@font-face {
		font-family: 'open';
		src: url('fonts/opensans-regular-webfont.eot');
		src: url('fonts/opensans-regular-webfont.eot?#iefix') format('embedded-opentype'),
		url('fonts/opensans-regular-webfont.woff2') format('woff2'),
		url('fonts/opensans-regular-webfont.woff') format('woff'),
		url('fonts/opensans-regular-webfont.ttf') format('truetype'),
		url('fonts/opensans-regular-webfont.svg#open_sansregular') format('svg');
		font-weight: 400;
		font-style: normal;

	}

	@font-face {
		font-family: 'open';
		src: url('fonts/opensans-semibold-webfont.eot');
		src: url('fonts/opensans-semibold-webfont.eot?#iefix') format('embedded-opentype'),
		url('fonts/opensans-semibold-webfont.woff2') format('woff2'),
		url('fonts/opensans-semibold-webfont.woff') format('woff'),
		url('fonts/opensans-semibold-webfont.ttf') format('truetype'),
		url('fonts/opensans-semibold-webfont.svg#open_sanssemibold') format('svg');
		font-weight: 700;
		font-style: normal;
	}

	@font-face {
		font-family: 'poster';
		src: url('fonts/posterizer_kg-webfont.eot');
		src: url('fonts/posterizer_kg-webfont.eot?#iefix') format('embedded-opentype'),
		url('fonts/posterizer_kg-webfont.woff2') format('woff2'),
		url('fonts/posterizer_kg-webfont.woff') format('woff'),
		url('fonts/posterizer_kg-webfont.ttf') format('truetype'),
		url('fonts/posterizer_kg-webfont.svg#posterizerkguploaded_file') format('svg');
		font-weight: 400;
		font-style: normal;
	}
</style>