<div id="counters" class="b-counters i-small-pd">
	<div class="i-inner-wrapper">
		<div class="b-title__descr b-title__descr_red">Мы уверены</div>
		<h2 class="b-title_small b-title_black">
			Время - ценный ресурс
		</h2>

		<p class="b-descr_small b-descr_dark">
			И мы знаем, как его спасти. <br />
			Скорость — наша суперспособность. <br />
			В любой проблеме мы видим возможность.
		</p>
	</div>
	
	<div class="b-counters__list">
		<div class="b-counters__item">
			<div class="b-counters__numbers js-counter" data-start="0" data-end="50">0</div>
			<div class="b-counters__descr">Сайтов и приложений<br />протестировано</div>
		</div>
		<div class="b-counters__item">
			<div class="b-counters__numbers">><span class="js-counter" data-start="0" data-end="8000">0</span></div>
			<div class="b-counters__descr">Багов найдено</div>
		</div>
		<div class="b-counters__item">
			<div class="b-counters__numbers js-counter" data-start="0" data-end="100000">0</div>
			<div class="b-counters__descr">Платных покупок внутри</div>
		</div>
		<div class="b-counters__item">
			<div class="b-counters__numbers js-counter" data-start="0" data-end="5000000">0</div>
			<div class="b-counters__descr">Установок без багов</div>
		</div>
	</div>

	<div class="i-btn-box">
		<button class="b-btn b-btn_green js-popup-control">Начать охоту за багами</button>
	</div>
</div>