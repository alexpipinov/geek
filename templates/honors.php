<section id="honors" class="b-honors i-small-pd">
	<div class="i-inner-wrapper">
		<div class="b-title__descr b-title__descr_red">Полюбуйтесь на</div>
		<h2 class="b-title_small b-title_black">
			Наши трофеи
		</h2>
	</div>

	<div class="b-honors__list b-grid">
		<article class="b-honors__item b-grid__item b-grid__4" style="background-image: url('images/honors/1.jpg')">
			<div class="b-honors__descr">
				<h3 class="b-honors__title">Farm Up</h3>
				<span class="b-honors__text">
					Business Strategy with city-builder elements set in 30's USA
				</span>

				<div class="b-honors__data">
					<span class="b-honors__devices">PC/iOS/Andorid</span>
					<span class="b-honors__count">1373</span>
					<span class="b-honors__errors">ошибок</span>
				</div>
			</div>
		</article>
		<article class="b-honors__item b-grid__item b-grid__4"  style="background-image: url('images/honors/2.jpg')">
			<div class="b-honors__descr">
				<h3 class="b-honors__title">Real Racing</h3>
				<span class="b-honors__text">
					Business Strategy with city-builder elements set in 30's USA
				</span>

				<div class="b-honors__data">
					<span class="b-honors__devices">PC/iOS/Andorid</span>
					<span class="b-honors__count">1373</span>
					<span class="b-honors__errors">ошибок</span>
				</div>
			</div>
		</article>
		<article class="b-honors__item b-grid__item b-grid__4" style="background-image: url('images/honors/1.jpg')" >
			<div class="b-honors__descr">
				<h3 class="b-honors__title">Hungry Shark World</h3>
				<span class="b-honors__text">
					Business Strategy with
				</span>

				<div class="b-honors__data">
					<span class="b-honors__devices">PC/iOS/Andorid</span>
					<span class="b-honors__count">1373</span>
					<span class="b-honors__errors">ошибок</span>
				</div>
			</div>
		</article>
		<article class="b-honors__item b-grid__item b-grid__4"  style="background-image: url('images/honors/2.jpg')">
			<div class="b-honors__descr">
				<h3 class="b-honors__title">My Talking Tom</h3>
				<span class="b-honors__text">
					Business Strategy with city-builder elements set in 30's USA Business Strategy with city-builder elements set in 30's USA Business Strategy with city-builder elements set in 30's USA
				</span>

				<div class="b-honors__data">
					<span class="b-honors__devices">PC/iOS/Andorid</span>
					<span class="b-honors__count">1373</span>
					<span class="b-honors__errors">ошибок</span>
				</div>
			</div>
		</article>
		<article class="b-honors__item b-grid__item b-grid__4"  style="background-image: url('images/honors/1.jpg')">
			<div class="b-honors__descr">
				<h3 class="b-honors__title">Hill Climb Racing</h3>
				<span class="b-honors__text">
					Business Strategy with city-builder elements set in 30's USA
				</span>

				<div class="b-honors__data">
					<span class="b-honors__devices">PC/iOS/Andorid</span>
					<span class="b-honors__count">1373</span>
					<span class="b-honors__errors">ошибок</span>
				</div>
			</div>
		</article>
	</div>
</section>