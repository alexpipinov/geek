var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var LiveReloadPlugin = require('webpack-livereload-plugin');
var autoprefixer = require('autoprefixer');

var NODE_ENV = process.env.NODE_ENV;

module.exports = {
	context: __dirname,

	entry: {
		build: ['./src/js/main.js'],
		styles: './src/less/styles.less'
	},

	output: {
		path: path.resolve('./build'),
		filename: '[name].js',
		publicPath: ''
	},

	module: {
		loaders: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				include: path.join(__dirname, '/src/js/'),
				loader: "babel"
			},
			{
				test:   /\.less$/,
				include: __dirname + '/src/less',
				loader: ExtractTextPlugin.extract('css?minimize!autoprefixer?browsers=last 2 version!resolve-url!less')
			},
			{
				test: /\.(jpg|jpeg|gif|png)$/,
				loader: 'url'
			},
			{test: /\.svg/, loader: 'svg-url-loader'},
			{
				test   : /\.(ttf|eot|svg|woff(2)?)(\?[a-z0-9=&.]+)?$/,
				loader : 'file-loader'
			}
		]
	},

	postcss: [
		autoprefixer({ browsers: ['last 2 versions'] })
	],

	watch: true,

	devtool: 'cheap-module-inline-source-map',

	plugins: [
		new webpack.NoErrorsPlugin(),
		new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /ru/),
		new ExtractTextPlugin('styles.css'),
		new LiveReloadPlugin({port: 35729})
	],

	resolve: {
		root: [
			path.resolve('./src/js')
		],
		extensions: ['', '.js']
	}
};

if (NODE_ENV === 'production') {
	module.exports.plugins.push(
		new webpack.optimize.UglifyJsPlugin({
			compress: {
				warnings: false
			},
			mangle: {
				except: ['exports', 'require']
			}
	 	})
	)
}
