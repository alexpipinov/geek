<!doctype html>
<html>
<head>
	<meta charset="utf-8" />
	<base href="/geek/" />
	<title>Geekstone</title>
	<meta name="description" content=""/>
	<meta name="keywords" content=""/>
	<meta name="author" content=""/>
	<meta name="copyright" content="(c)">
	<meta http-equiv="Reply-to" content="alexpipinov@gmail.com">
	<meta name="viewport" content="width=device-width,  initial-scale=1.0, user-scalable=no, maximum-scale=1.0"/>

	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<meta name="format-detection" content="telephone=no">
	<meta name="msapplication-tap-highlight" content="no" />
	<meta name="HandheldFriendly" content="True"/>
	<meta http-equiv="cleartype" content="on"/>

	<? include_once 'templates/favicons/favicons.php' ?>
	<? include_once 'templates/fonts.php' ?>
	
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
	<link rel="stylesheet" href="build/styles.css">
</head>

<body>
	<!--	FLOW HEADER -->
	<header class="b-header js-header b-header_type_flow">
		<? include 'templates/common/headerContent.php' ?>
	</header>

	<!-- STATIC HEADER -->
	<header id="staticHeader" class="b-header js-header b-header_type_static">
		<? include 'templates/common/headerContent.php' ?>

		<a id="hamburger-icon" href="#">
			<span class="line line-1"></span>
			<span class="line line-2"></span>
			<span class="line line-3"></span>
		</a>
	</header>
	
	<div id="page" class="i-page-wrapper">
		<!-- FIRST ROW -->
		<? include_once 'templates/firstRow.php' ?>

		<!-- SECOND ROW -->
		<? include_once 'templates/secondRow.php' ?>

		<!-- DUO -->
		<? include_once 'templates/duo.php' ?>

		<!-- APP TESTING -->
		<? include_once 'templates/appTesting.php' ?>

		<!--	Mission	-->
		<? include_once 'templates/mission.php' ?>

		<!--	Kinds of testing	-->
		<? include_once 'templates/testKinds.php' ?>

		<!--	Counters	-->
		<? include_once 'templates/counters.php' ?>

		<!--	Honors	-->
		<? include_once 'templates/honors.php' ?>

		<!--	Feedback	-->
		<? include_once 'templates/feedback.php' ?>

		<!--	Map	-->
		<? include_once 'templates/map.php' ?>

		<!--	Footer	-->
		<? include_once 'templates/common/footer.php' ?>
	</div>


<!-- Popup		-->
<? include_once 'templates/popup.php' ?>

<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script src="build/build.js"></script>
<script src="http://localhost:35729/livereload.js"></script>
</body>
</html>