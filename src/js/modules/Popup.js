class Popup {
	constructor() {
		this.el = document.getElementById('popup');
		this.form = document.getElementById('order');
		this.submitBtn = document.getElementById('orderSubmitBtn');
		this.errorMsg = document.getElementById('formErrorMsg');
		this.requiredFields = this.form.querySelectorAll('[data-required]');

		this.data = {};
		this.isOpened = false;
		this.timeout = null

		this.subscribers();
	}

	toggleState() {
		if (this.timeout) return;

		if (!this.isOpened) {
			this.el.classList.add('_is-active');

			this.timeout = setTimeout(() => {
				this.el.classList.add('_is-visible');
				document.body.classList.add('i-overflow');
				this.timeout = null;
			}, 200);

			this.isOpened = true;
		}else {
			this.el.classList.remove('_is-visible');

			this.timeout = setTimeout(() => {
				this.el.classList.remove('_is-active');
				document.body.classList.remove('i-overflow');
				this.timeout = null;
			}, 400);

			this.resetAllErrors();
			this.errorMsg.classList.remove('_is-active');
			this.isOpened = false;
		}
	}

	handleSubmit(e) {
		this.validate();
	}

	validate() {
		let errorCounter = 0;

		for (let i = 0; i < this.requiredFields.length; i++) {
			let input = this.requiredFields[i];

			if (!input.value) {
				this.markFieldAsIncorrect(input);
				errorCounter++;
			}else {
				this.data[input.name] = input.value;
			}
		}

		if (errorCounter) {
			errorCounter = 0;
		}else {
			this.sendRequest();
		}
	}

	markFieldAsIncorrect(field) {
		field.classList.add('_is-error');
		this.errorMsg.classList.add('_is-active');
	}

	resetAllErrors() {
		for(let i = 0; i < this.requiredFields.length; i++) {
			this.requiredFields[i].classList.remove('_is-error');
		}
	}

	resetError(e) {
		e.target.classList.remove('_is-error');
		console.log(this.errorMsg);
		this.errorMsg.classList.remove('_is-active');
	}

	sendRequest() {
		//ЗАПРОС НА СЕРВЕР ПИШЕМ ЗДЕСЬ
		//В ЗАПРОСЕ ОТПРАВЛЯЕМ this.data


		//ПРИ УСПЕШНОЙ ОТПРАВКЕ ПИШЕМ
		this.form.classList.add('_is-success');
	}

	handleClick(e) {
		let target = e.target;

		// цикл двигается вверх от target к родителям до table
		while (target != document.body) {
			if (target.classList.contains('js-popup-control' || target.classList.contains('js-popup-close'))) {
				e.preventDefault();
				this.toggleState();
				return;
			}
			target = target.parentNode;
		}
	}

	subscribers() {
		document.addEventListener('click', this.handleClick.bind(this));
		this.submitBtn.addEventListener('click', this.handleSubmit.bind(this));

		for (let i = 0; i < this.requiredFields.length; i++) {
			this.requiredFields[i].addEventListener('focus', this.resetError.bind(this));
		}
	}
}

export default Popup;