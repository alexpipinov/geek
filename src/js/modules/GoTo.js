import scrollTo from './helpers/scrollTo';

class GoTo {
	constructor() {
		this.el = document.querySelectorAll('.js-go-to');
		this.relBox = document.getElementById('staticHeader');
		this.relBoxHeight = this.getRelBoxHeight();

		this.header = document.getElementById('staticHeader');
		this.ham = document.getElementById('hamburger-icon');
		this.nav = this.header.querySelectorAll('.js-nav');

		this.subscribers();
	}

	getRelBoxHeight() {
		return this.relBox.offsetHeight;
	}

	hideNav() {
		this.ham.classList.remove('_is-active');

		for (let i = 0; i < this.nav.length; i++) {
			this.nav[i].classList.remove('_is-active');
		}
	}

	getCoords(elem) {
		var box = elem.getBoundingClientRect();

		return {
			top: box.top + pageYOffset,
			left: box.left + pageXOffset
		};
	}

	handleClick(e) {
		e.preventDefault();

		let link = e.currentTarget;
		let targetNode = document.getElementById(link.getAttribute('data-target'));
		let pos = this.getCoords(targetNode).top - this.relBoxHeight + 20;

		this.hideNav();
		scrollTo(pos, 3000);
	}

	subscribers() {
		for (let i = 0; i < this.el.length; i++) {
			this.el[i].addEventListener('click', this.handleClick.bind(this));
		}

		window.addEventListener('resize', this.getRelBoxHeight.bind(this));
	}
}

export default GoTo;
