import Counter from './Counter';

export default class Counters {
	constructor() {
		this.el = document.getElementById('counters');
		this.elPosY = this.getCoords(this.el).top;

		this.counterNodes = document.querySelectorAll('.js-counter');
		this.countersList = [];

		this.initCounters();
		this.initEvents();
		this.handleScroll();
	}
	
	initCounters() {
		for (let i = 0; i < this.counterNodes.length; i++) {
			let node = this.counterNodes[i];
			let startValue = node.getAttribute('data-start');
			let endValues = node.getAttribute('data-end');

			this.countersList.push(
				new Counter(this.counterNodes[i], startValue, endValues, 2, {
					useEasing : false,
					useGrouping : false,
					separator : '',
					decimal : '',
					prefix : '',
					suffix : ''
				})
			)
		}
	}

	getCoords(elem) {
		var box = elem.getBoundingClientRect();

		return {
			top: box.top + pageYOffset,
			left: box.left + pageXOffset
		};
	}


	handleScroll() {
		if (pageYOffset >= this.elPosY && !this.el.classList.contains('_is-shown')) {
			this.el.classList.add('_is-shown');

			this.countersList.map((counter) => {
				counter.start();
			});
		}
	}

	initEvents() {
		window.addEventListener('scroll', this.handleScroll.bind(this));
	}
}