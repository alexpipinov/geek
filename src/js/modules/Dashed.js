export default class Dashed {
	constructor(container) {
		this.el = container;
		this.elYPos = this.getCoords(this.el);
		this.relBox = document.getElementById('staticHeader');
		this.relBoxHeight = this.relBox.offsetHeight;

		this.subscribers();
	}

	handleResize() {
		this.elYPos = this.getCoords(this.el);
		this.relBoxHeight = this.relBox.offsetHeight;
	}

	getCoords(elem) {
		var box = elem.getBoundingClientRect();

		return box.top + pageYOffset;
	}

	handleScroll(e) {
		if (window.pageYOffset >= (this.elYPos - this.relBoxHeight) && !this.el.classList.contains('_is-shown')) {
			this.el.classList.add('_is-shown');
		}
	}
	
	subscribers() {
		window.addEventListener('scroll', this.handleScroll.bind(this));
		window.addEventListener('resize', this.handleResize.bind(this));
	}
}