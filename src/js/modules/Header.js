export default class FlowHeader {
	constructor() {
		this.el = document.querySelectorAll('.js-header');
		this.staticHeaderHeight = document.getElementById('staticHeader').offsetHeight;
		this.relBox = document.getElementById('firstRow');
		this.relBoxHeight = this.getRelBoxHeight();

		this.handleScroll = this.handleScroll.bind(this);
		this.handleResize = this.handleResize.bind(this);

		this.initEvents();
	}

	getRelBoxHeight() {
		return this.relBox.offsetHeight - this.staticHeaderHeight;
	}

	handleResize() {
		this.relBoxHeight = this.getRelBoxHeight();
	}

	handleScroll(e) {
		if (window.pageYOffset > this.relBoxHeight) {
			for (let i = 0; i < this.el.length; i++) {
				this.el[i].classList.add('_is-active');
			}
		}else {
			for (let i = 0; i < this.el.length; i++) {
				this.el[i].classList.remove('_is-active');
			}
		}
	}

	initEvents() {
		window.addEventListener('scroll', this.handleScroll);
		window.addEventListener('resize', this.handleResize);
	}
}