function GMap(mapNode, Lat, Lng, zoom) {
	if (!(this instanceof GMap)) {
		new GMap(mapNode, Lat, Lng, zoom);
	}

	this.coordinates = new google.maps.LatLng(Lat, Lng);

	this.image = {
		url: 'images/mapMarker.png',
		// This marker is 20 pixels wide by 32 pixels tall.
		size: new google.maps.Size(55, 79),
		// The origin for this image is 0,0.
		origin: new google.maps.Point(0,0),
		// The anchor for this image is the base of the flagpole at 0,32.
		anchor: new google.maps.Point(28, 79)
	};

	this.mapNode = mapNode;
	this.options = {
		zoom: zoom || 17,
		center: this.coordinates,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		disableDefaultUI	  : true,
		// настройки контролов
		streetViewControl: false,
		scrollwheel: false,
		zoomControl: false,
		draggable: true,
		styles: [
			{
				"stylers": [
					{ "saturation": -93 },
					{ "lightness": 5 }
				]
			}
		]
	};

	this.initialize();
}

/**
 * Set map marker to position
 */
GMap.prototype.setMarker = function() {
	this.marker = new google.maps.Marker({
		position: this.coordinates,
		map: this.mapInstance,
		draggable: false,
		icon: this.image
	});
};

GMap.prototype.createMap = function() {
	this.mapInstance = new google.maps.Map(this.mapNode, this.options);
};



GMap.prototype.initialize = function() {
	this.createMap();
	this.setMarker();
};

GMap.prototype.destroy = function() {

};


module.exports =  GMap;