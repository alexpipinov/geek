class Ham {
	constructor() {
		this.header = document.getElementById('staticHeader');
		this.ham = document.getElementById('hamburger-icon');
		this.nav = this.header.querySelector('.js-nav');

		this.initSubscribers();
	}

	handleClick(e) {
		e.preventDefault();
		let ham = e.currentTarget;

		if (ham.classList.contains('_is-active')) {
			ham.classList.remove('_is-active');
			this.nav.classList.remove('_is-active');
		}else {
			ham.classList.add('_is-active');
			this.nav.classList.add('_is-active');
		}
	}

	initSubscribers() {
		this.ham.addEventListener('click', this.handleClick.bind(this));
	}
}

export default Ham;