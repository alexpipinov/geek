import GoTo from './modules/GoTo';
import Header from './modules/Header';
import Map from './modules/Map';
import Counters from './modules/Counters';
import Dashed from './modules/Dashed';
import Popup from './modules/Popup';

import Ham from './modules/Ham';

import FastClick from 'fastclick';

let app = {
	initMap: function() {
		let map = document.getElementById('map');
		let lat = map.getAttribute('data-lat');
		let long = map.getAttribute('data-long');

		google.maps.event.addDomListener(window, 'load', () => {
			new Map(map, lat, long, 17);
		});
	},

	initDasheds: function() {
		new Dashed(document.getElementById('remoteTesting'));
		new Dashed(document.getElementById('mission'));
	},

	initModules: function() {
		new GoTo();
		new Header();
		new Ham();
		new Counters();
		new Popup();

		this.initMap();
		this.initDasheds();

		FastClick.attach(document.body);
	},

	start: function() {
		this.initModules()
	}
};

export default app;